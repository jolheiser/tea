module code.gitea.io/tea

go 1.12

require (
	code.gitea.io/sdk/gitea v0.11.0
	github.com/araddon/dateparse v0.0.0-20190622164848-0fb0a474d195
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-gitea/yaml v0.0.0-20170812160011-eb3733d160e7
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/stretchr/testify v1.4.0
	github.com/urfave/cli/v2 v2.1.1
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
